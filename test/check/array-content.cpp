#include "flacl/vector.h"
#include <optional>

#include <cassert>

int main() {
  flacl::Array<std::size_t,6> fa;
  fa.resize(4,42);
  assert(fa == (flacl::Array<std::size_t,6>{42,42,42,42}));
  fa[0] = 10;
  fa[1] = 11;
  fa[2] = 12;
  fa[3] = 13;
  for (std::size_t i = 0; i < 4; ++i) {
    assert(fa[i] == (10+i));
  }

  assert(*fa.begin() == 10);
  assert(*fa.rbegin() == 13);
  assert(fa.begin()[2] == 12);

  flacl::Array<std::size_t,6> const fa2 = {10,11,12,13};
  for (std::size_t i = 0; i < 4; ++i) {
    assert(fa[i] == fa2[i]);
  }

  assert(fa == fa2);

  auto const empl = fa.emplace(fa.begin() + 2,100);
  assert((empl - fa.begin()) == 2);

  assert(fa != (flacl::Array<std::size_t,6>{10,100,11,12,13}));
  assert(fa != (flacl::Array<std::size_t,6>{10,11,100,12}));
  assert(fa != (flacl::Array<std::size_t,6>{10,11,100,12,13,14}));
  assert(fa == (flacl::Array<std::size_t,6>{10,11,100,12,13}));

  fa.emplace_back(1000);

  assert(fa == (flacl::Array<std::size_t,6>{10,11,100,12,13,1000}));
  assert(fa.emplace_back(0) == fa.end());
  assert(fa.size() == 6);

  fa.erase((fa.begin()+2),(fa.begin()+4));
  assert(fa == (flacl::Array<std::size_t,6>{10,11,13,1000}));

  flacl::Array<std::size_t,6> fa3 = std::move(fa);

  assert(fa.empty());
  assert(fa3 == (flacl::Array<std::size_t,6>{10,11,13,1000}));

  fa3.pop_back();
  fa3.back()--;

  assert(fa3 == (flacl::Array<std::size_t,6>{10,11,12}));
  assert((flacl::Array<std::size_t,4>(fa3.begin(),fa3.end())) == (flacl::Array<std::size_t,10>(fa3.begin(),fa3.end())));

  fa3.resize(2);
  assert(fa3 == (flacl::Array<std::size_t,6>{10,11}));


  flacl::Array<std::optional<int>,100> fopt = {std::nullopt,7,13};
  fopt.pop_back();
  fopt.emplace_back(19);
  fopt.insert(fopt.begin()+1,4,std::nullopt);
  assert(fopt.size() == 7);
  assert(fopt.at(3) == std::nullopt);

  flacl::Array<std::optional<int>,13> const fopt2(fopt.begin(),fopt.end());
  assert(fopt.insert(fopt.begin(),fopt2.begin(),fopt2.begin()) == fopt.begin());
  fopt = fopt;
  assert(fopt == fopt2);
  fopt = std::move(fopt);
  assert(fopt == fopt2);
  assert(fopt.resize(fopt.size()));
  assert(fopt == fopt2);

  assert(fopt.resize(fopt.capacity(),std::nullopt));
  assert(fopt.size() == fopt.capacity());
  assert(fopt.push_back(9) == fopt.end());
  assert(fopt.push_back(fopt2.front()) == fopt.end());
  assert(fopt.emplace(fopt.begin(),9) == fopt.end());

  assert(fopt.erase(fopt.begin(),fopt.end()+1) == fopt.end());
  assert(fopt.erase(fopt.end(),fopt.begin()) == fopt.end());
  assert(fopt.erase(fopt.begin()-1,fopt.begin()) == fopt.end());
  assert(fopt.erase(fopt.begin(),fopt.begin()) == fopt.begin());
}
