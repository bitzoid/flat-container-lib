#include "flacl/vector.h"

#include <cassert>

int main() {
  flacl::Array<int,6> fa;
  assert(fa.empty());
  assert(fa.max_size() == 6);
  assert(fa.capacity() == 6);
  assert(fa.size() == 0);

  fa.emplace_back(0);
  assert(!fa.empty());
  assert(fa.size() == 1);
  fa.emplace_back(0);
  fa.emplace_back(0);
  assert(fa.size() == 3);
  fa.emplace_back(0);
  fa.emplace_back(0);
  fa.emplace_back(0);
  assert(fa.size() == 6);
  fa.emplace_back(0);
  assert(fa.size() == 6);

  fa.pop_back();
  assert(fa.size() == 5);
  fa.pop_back();
  fa.pop_back();
  fa.pop_back();
  fa.pop_back();
  assert(fa.size() == 1);
  fa.pop_back();
  assert(fa.size() == 0);
  fa.pop_back();
  assert(fa.size() == 0);
  assert(fa.empty());

  assert(fa.resize(4));
  assert(fa.size() == 4);
  assert(!fa.resize(8));
  assert(fa.size() == 4);
  assert(fa.resize(0));
  assert(fa.size() == 0);

}
