#include "flacl/set.h"
#include <optional>

#include <cassert>

template <typename T, std::size_t N, typename Equal, typename Range>
static bool equal_set(flacl::UnorderedSet<T,N,Equal> const& us, Range const& range) {
  for (auto it = std::begin(range); it != std::end(range); ++it) {
    if (!us.contains(*it)) return false;
  }
  for (auto const& t: us) {
    bool found = false;
    for (auto it = std::begin(range); it != std::end(range); ++it) {
      if (*it == t) {
        found = true;
        break;
      }
    }
    if (!found) return false;
  }
  return true;
}

int main() {
  flacl::UnorderedSet<std::optional<int>,6> us{42,0,42,std::nullopt};
  {
    assert(equal_set(us,std::vector<std::optional<int>>{std::nullopt,0,42}));
    assert(!equal_set(us,std::vector<std::optional<int>>{std::nullopt,0}));
    assert(!equal_set(us,std::vector<std::optional<int>>{std::nullopt,0,-1}));
    assert(equal_set(us,std::vector<std::optional<int>>{std::nullopt,0,0,0,0,42}));
    assert((us == flacl::UnorderedSet<std::optional<int>,3>{std::nullopt,0,42}));
    assert((us != flacl::UnorderedSet<std::optional<int>,2>{}));
    assert((flacl::UnorderedSet<std::optional<int>,3>{0,1} != flacl::UnorderedSet<std::optional<int>,3>{0,2}));
    assert((flacl::UnorderedSet<std::optional<int>,3>{0,1} == flacl::UnorderedSet<std::optional<int>,3>{1,0}));
  }
  {
    assert(us.contains(std::nullopt));
    assert(us.contains(42));
    assert(!us.contains(-1));
  }
  assert(us.flat_size() == 6);
}
