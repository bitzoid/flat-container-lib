#include "flacl/vector.h"

int main() {
  flacl::detail::Allocator<int,3,std::allocator> alloc;
  flacl::detail::Allocator<int,3,std::allocator> alloc2(alloc);
  flacl::detail::Allocator<int,3,std::allocator> alloc3(std::move(alloc2));
  alloc = alloc3;
  alloc = std::move(alloc3);

  alloc.deallocate(alloc.allocate(1),1);
  alloc.deallocate(alloc.allocate(9),9);

  alloc == alloc3;
  alloc != alloc3;
}
