#include "flacl/vector.h"

#include <cassert>

int main() {
  flacl::Vector<int,6> v = {0,1,2,3};

  {
    flacl::Vector<int,6> v2 = v;
    assert(v2 == v);
    v.push_back(4);
    assert(v2 != v);

    v.push_back(5);
    assert(v.size() == 6);
    assert(v.is_flat());
    v.push_back(6);
    assert(v.size() == 7);
    assert(!v.is_flat());
    assert(!flacl::flatten(v));

    v.pop_back();
    assert(flacl::flatten(v));
    assert(v.is_flat());

    v2 = v;
    assert(v.is_flat());
    assert(v2.is_flat());

    /* TEST: copy non-flat to flat */
    v.push_back(6);
    assert(!v.is_flat());
    assert(v2.is_flat());

    v2 = v;
    assert(v2 == v);
    assert(!v.is_flat());
    assert(!v2.is_flat());

    /* TEST: copy non-flat to non-flat */
    v2 = v;
    assert(v2 == v);
    assert(!v.is_flat());
    assert(!v2.is_flat());

    /* TEST: copy flat to non-flat */
    v.pop_back();
    assert(flacl::flatten(v));
    assert(v.is_flat());
    assert(!v2.is_flat());

    v2 = v;
    assert(v2 == v);
    assert(v.is_flat());
    assert(v2.is_flat());

    /* TEST: copy flat to flat */
    v2 = v;
    assert(v2 == v);
    assert(v.is_flat());
    assert(v2.is_flat());
  }

  v.push_back(6);

  assert((flacl::Vector<int,6>{0,1,2,3,4,5,6} == v));

  /* TEST: move non-flat to empty */
  {
    auto v3 = v;
    flacl::Vector<int,6> v4;
    assert(!v3.is_flat());
    assert(v4.is_flat());

    v4 = std::move(v3);
    assert(v3.size() == 0);
    assert(v3.is_flat());
    assert(!v4.is_flat());
    assert(v4.size() == 7);
  }

  /* TEST: move non-flat to flat */
  {
    auto v3 = v;
    flacl::Vector<int,6> v4 = {0};
    assert(!v3.is_flat());
    assert(v4.is_flat());

    v4 = std::move(v3);
    assert(v3.size() == 0);
    assert(v3.is_flat());
    assert(!v4.is_flat());
  }

  /* TEST: move non-flat to non-flat */
  {
    auto v3 = v;
    flacl::Vector<int,6> v4 = v3;
    v3.push_back(7);
    assert(!v3.is_flat());
    assert(!v4.is_flat());

    v4 = std::move(v3);
    assert(v3.size() == 0);
    assert(v3.is_flat());
    assert(!v4.is_flat());
    assert(v4.size() == 8);
  }

  v.resize(6);
  assert(flacl::flatten(v));
  assert((v == flacl::Vector<int,6>{0,1,2,3,4,5}));

  /* TEST: move flat to non-flat */
  {
    auto v3 = v;
    flacl::Vector<int,6> v4 = v3;
    v4.push_back(6);
    assert(v3.is_flat());
    assert(!v4.is_flat());

    v4 = std::move(v3);
    assert(v3.size() == 0);
    assert(v3.is_flat());
    assert(v4.is_flat() || !v4.is_flat()); // not requirements on flattening
    assert(v4.size() == 6);
    assert(flacl::flatten(v4));
  }

  /* TEST: swap two flats */
  {
    auto v3 = v;
    auto v4 = v;
    v4.pop_back();
    assert(v3.is_flat());
    assert(v4.is_flat());

    v3.swap(v4);
    assert(v3.is_flat());
    assert(v4.is_flat());
    assert(v3 != v);
    assert(v4 == v);
  }

  /* TEST: swap two non-flats */
  {
    auto v3 = v;
    auto v4 = v;
    v4.push_back(100);
    v3.push_back(200);
    assert(!v3.is_flat());
    assert(!v4.is_flat());

    v3.swap(v4);
    assert(!v3.is_flat());
    assert(!v4.is_flat());
    assert(v3 != v);
    assert(v4 != v);
    assert(v4 != v3);
  }

  /* TEST: swap non-flat and flat */
  {
    auto v3 = v;
    auto v4 = v;
    v4.push_back(100);
    assert(v3.is_flat());
    assert(!v4.is_flat());

    v3.swap(v4);
    assert(!v3.is_flat());
    assert(flacl::flatten(v4));
    assert(v3 != v);
    assert(v4 == v);
    assert(v4 != v3);
  }

  /* TEST: swap flat and non-flat */
  {
    auto v3 = v;
    auto v4 = v;
    v4.push_back(100);
    assert(v3.is_flat());
    assert(!v4.is_flat());

    v4.swap(v3);
    assert(!v3.is_flat());
    assert(flacl::flatten(v4));
    assert(v3 != v);
    assert(v4 == v);
    assert(v4 != v3);
  }

}
