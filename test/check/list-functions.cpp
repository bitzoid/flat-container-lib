#include "flacl/list.h"

// this test does not actually check correctness but enumerates all API functions for gcov

int main() {
  flacl::List<int> fl;
  //{
  //  flacl::List<int> a1;
  //  int data[] = {0,1};
  //  flacl::List<int> a2(std::begin(data),std::end(data));
  //  flacl::FwdVector<int> a3({0,1});

  //  flacl::List<int> a4(a3);
  //  flacl::List<int> a5(std::move(a4));

  //  a4 = std::move(a5);
  //  a3 = a4;
  //  a2.assign(std::begin(data),std::end(data));
  //  a3.assign({0,1});
  //}

  flacl::List<int> a({0,1,2,3,4,5,6,7,8,9});
  flacl::List<int> const ac(a);
  { /* element access */
    a.front() = ac.front();
    a.back() = ac.back();
  }

  { /* iterators */
    auto it = a.begin();
    auto itc = ac.begin();
    auto itcc = ac.cbegin();
    for (; it != a.end() && itc != ac.end() && itcc != ac.cend(); ++it, ++itc, ++itcc) {
      *it = *itc;
      *it = *itcc;
    }
  }

  { /* capacity */
    a.capacity();
    a.reserve(0);
    (void)a.empty();
    a.size();
  }

  { /* modifiers */
    a.clear();
    int i = 0;
    a.insert(a.end(),0);
    a.insert(a.end(),std::move(i));
    a.emplace(a.end(),0);
    a.insert_after(a.begin(),0);
    a.insert_after(a.begin(),std::move(i));
    a.erase(a.begin());
    a.erase(a.begin(),a.end());
    a.push_back(0);
    a.push_back(std::move(i));
    a.emplace_back(0);
    a.push_front(0);
    a.push_front(std::move(i));
    a.emplace_front(0);
    a.pop_front();
    a.pop_back();
    a.resize(2);
    a.resize(10,0);
    auto b = ac;
    std::swap(a,b);
    a.swap(b);
  }

  { /* comparison functions */
    a == ac;
    a != ac;
    a < ac;
    a <= ac;
    a > ac;
    a >= ac;
  }

  { /* iterator functions */
    auto it = a.begin();
    it == it;
    it != it;
    *it;
    ++it;
    it++;
  }
}

