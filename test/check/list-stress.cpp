#include "flacl/list.h"

#include <list>
#include <random>

#include <cassert>

namespace {
  template <typename Os, typename T>
  Os& operator<<(Os& os, flacl::List<T> const& list) {
    os << "FL (size: " << list.size() << ")\t";
    for (auto it = list.cbegin(); it != list.cend(); ++it) {
      os << *it << "\t";
    }
    return os;
  }
  template <typename Os, typename T>
  Os& operator<<(Os& os, std::list<T> const& list) {
    os << "SL (size: " << list.size() << ")\t";
    for (auto it = list.cbegin(); it != list.cend(); ++it) {
      os << *it << "\t";
    }
    return os;
  }

  template <typename T>
  bool equal(std::list<T> const& sl, flacl::List<T> const& fl) {
    if (sl.size() != fl.size()) return false;
    return std::equal(sl.begin(),sl.end(),fl.begin(),fl.end());
  }
}

int main() {
  using T = short;
  auto insert = [](auto& list, std::size_t i, T val) {
    auto it = std::next(list.begin(), i % (list.size()+1));
    return *list.insert(it,val);
  };
  auto erase = [](auto& list, std::size_t i, T& val) {
    if (list.size()) {
      auto it = std::next(list.begin(), i % list.size());
      assert(it != list.end());
      val = *it;
      if (auto eit = list.erase(it); eit != list.end()) {
        return *eit;
      }
    }
    return T{0};
  };
  auto erase_range = [](auto& list, std::size_t i, std::size_t j, T& val) {
    if (list.size()) {
      i %= list.size();
      j %= (list.size()+1);
      if (j < i) std::swap(i,j);
      auto it = std::next(list.begin(), i);
      auto ite = std::next(list.begin(), j);
      assert(it != list.end());
      val = *it;
      if (auto eit = list.erase(it,ite); eit != list.end()) {
        return *eit;
      }
    }
    return T{0};
  };
  std::list<T> sl;
  flacl::List<T> fl;
  std::mt19937 mt(42);
  enum Ops : int {
    op_insert = 2, // and higher
    op_erase = 1,
    op_erase_range = 0
  };
  auto op = [&](Ops ins) {
    std::size_t const i = mt();
    std::size_t const j = mt();
    T const val = std::uniform_int_distribution<T>()(mt);
    T vsl = val;
    T vfl = val;
    switch (ins) {
      case op_erase_range: {
        auto const sx = erase_range(sl,i,j,vsl);
        auto const fx = erase_range(fl,i,j,vfl);
        assert(sx == fx);
      } break;
      case op_erase: {
        assert(erase(sl,i,vsl) == erase(fl,i,vfl));
      } break;
      default:
      case op_insert: {
        assert(insert(sl,i,vsl) == insert(fl,i,vfl));
      } break;
    }
    assert(fl.invariant());
    return vsl==vfl;
  };
  auto check = [&](std::size_t num, auto gen) {
    for (auto i = num; i--; ) {
      assert(op(static_cast<Ops>(gen(mt))));
      assert(std::next(sl.begin(),sl.size()) == sl.end());
      assert(std::next(fl.begin(),fl.size()) == fl.end());
      assert(equal(sl,fl));
    }
  };
  std::size_t const lim = 1024;
  check(lim,[](auto){return op_insert;});
  check(lim+1,[](auto){return op_erase;});
  check(2*lim,std::uniform_int_distribution<int>(0,10));
}
