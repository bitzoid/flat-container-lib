#include "flacl/set.h"

// this test does not actually check correctness but enumerates all API functions for gcov

int main() {
  flacl::UnorderedSet<int,6> us{42};
  flacl::UnorderedSet<int,6> const cus{42};
  {
    flacl::UnorderedSet<int,6> empty;
    auto copy = us;
    auto moved = std::move(copy);
    copy = std::move(moved);
    moved = copy;
    flacl::UnorderedSet<int,6> init{0,4,2,4};
    init = {0,1,2,3};
    flacl::UnorderedSet<int,6> iter(us.begin(),us.end());
  }
  { /* iterators */
    us.begin();
    cus.begin();
    us.cbegin();
    us.end();
    cus.end();
    us.cend();
  }
  { /* capacity */
    cus.empty();
    cus.size();
    cus.flat_size();
    cus.is_flat();
    cus.flat_size();
    cus.find(0);
    cus.contains(0);
    cus.count(0);
  }
  { /* modifiers */
    us.clear();
    us.swap(us);
    swap(us,us);
    us.erase(us.end());
    us.erase(100);
    us.insert(0);
    us.emplace(0);
    us.insert_nocheck(55);
    us.emplace_nocheck(77);
  }
  { /* comparison functions */
    us == cus;
    us != cus;
  }
  flatten(us);
}
