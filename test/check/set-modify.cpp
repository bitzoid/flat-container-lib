#include "flacl/set.h"
#include <optional>

#include <cassert>

int main() {
  flacl::UnorderedSet<std::optional<int>,6> us{42,0,42,std::nullopt};
  {
    assert(us.size() == 3);
    auto const end = us.end();
    assert(us.erase(end) == end);
    assert(us.size() == 3);
    assert(us.erase(-1) == 0);
  }
  {
    auto copy = us;
    assert(copy.size() == 3);
    copy.erase(42);
    assert(copy.size() == 2);
    assert(copy != us);
  }
  {
    auto copy = us;
    assert(copy.size() == 3);
    copy.erase(copy.begin());
    assert(copy.size() == 2);
    assert(copy != us);
  }
  {
    auto copy = us;
    assert(!copy.empty());
    assert(copy.size() == 3);
    copy.erase(std::prev(copy.end()));
    assert(copy.size() == 2);
    copy.erase(std::prev(copy.end()));
    assert(copy.size() == 1);
    copy.erase(std::prev(copy.end()));
    assert(copy.size() == 0);
    assert(copy.empty());
  }
  us.clear();
  for (int i = 0; i < 40; ++i) {
    assert(us.contains(i) == false);
    assert(us.insert(i).second == true);
    assert(us.contains(i) == true);
  }
  assert(us.size() == 40);
  for (int i = 0; i < 40; ++i) {
    assert(us.insert(i).second == false);
  }
  assert(us.size() == 40);
  for (int i = 0; i < 40; ++i) {
    assert(us.emplace(i).second == false);
  }
  assert(us.emplace(50).second == true);
  {
    auto const it = us.insert_nocheck(51);
    assert(it != us.end());
  }
  {
    auto const it = us.emplace_nocheck(52);
    assert(it != us.end());
  }
  assert(us.count(100) == 0);
  assert(us.count(50) == 1);
}
