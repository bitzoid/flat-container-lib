#include "flacl/vector.h"

#include <cassert>

#ifdef __cpp_exceptions

  struct Shy {
    // throws on every attempt to construct, except for nullptr
    Shy() { throw 0; }
    Shy(Shy const&) : Shy() {}
    Shy(Shy&&) : Shy() {}
    Shy& operator=(Shy const&) { throw 0; }
    Shy& operator=(Shy&&) { throw 0; }
    template <typename T>
    Shy(T&&) : Shy() {}
    Shy(std::nullptr_t) {}
  };

#endif

int main() {
  flacl::Array<std::size_t,6> a = {0,1,2};
  {
    flacl::Array<std::size_t,6> fa = a;
    std::array<std::size_t,100> huge{0};
    auto const e = fa.end();

    auto failed = fa.insert(fa.end(),huge.begin(),huge.end());
    assert(failed == e);
    assert(fa.end() == e);

    failed = fa.insert(fa.end(),100,0);
    assert(failed == e);
    assert(fa.end() == e);

    failed = fa.insert(fa.end(),huge.end(),huge.begin());
    assert(failed == e);
    assert(fa.end() == e);
  }

  {
    flacl::Array<std::size_t,6> fa = a;
    #ifdef __cpp_exceptions
    {
      bool threw = false;
      try {
        auto const copy = fa;
        copy.at(copy.size());
      } catch (...) {
        threw = true;
      }
      assert(threw);
    }
    {
      bool threw = false;
      try {
        fa.at(fa.size()) = 7;
      } catch (...) {
        threw = true;
      }
      assert(threw);
    }
    #endif
  }

  {
    flacl::Array<std::size_t,6> fa = a;
    auto const e = fa.end();
    auto const failed = fa.insert(fa.end()+1,0);
    assert(failed == e);
  }

  #ifdef __cpp_exceptions
  {
    Shy data[] = {nullptr};
    {
      bool thrown = false;
      try {
        flacl::Array<Shy,2> shy(std::begin(data),std::end(data));
      } catch (int) {
        thrown = true;
      }
      assert(thrown);
    }
    {
      bool thrown = false;
      try {
        flacl::Array<Shy,2> shy({nullptr});
      } catch (int) {
        thrown = true;
      }
      assert(thrown);
    }

    flacl::Array<Shy,10> shy;
    shy.emplace_back(nullptr);
    shy.emplace_back(nullptr);
    {
      bool thrown = false;
      try {
        flacl::Array<Shy,10> copy = shy;
      } catch (int) {
        thrown = true;
      }
      assert(thrown);
    }
    {
      bool thrown = false;
      try {
        flacl::Array<Shy,10> move = std::move(shy);
      } catch (int) {
        thrown = true;
      }
      assert(thrown);
    }
    {
      bool thrown = false;
      flacl::Array<Shy,10> copy;
      copy = flacl::Array<Shy,10>();
      try {
        copy = shy;
      } catch (int) {
        thrown = true;
      }
      assert(thrown);
    }
  }
  #endif

}
