#include "flacl/list.h"

#include <cassert>
#include <iostream>

int main() {
  { // construction, equality
    flacl::List<int> a(std::initializer_list<int>{});
    assert(a.empty());
    flacl::List<int> b;
    assert(a == b);
    assert(a.size() == 0);
    assert(a.invariant());
  }
  { // insertion, equality
    flacl::List<int> a({7,8,9});
    assert(a.size() == 3);
    assert(a.front() == 7);
    assert(a.back() == 9);
    assert(*a.begin() == 7);
    a.insert(a.end(),17);
    assert(a.invariant());
    assert(a.size() == 4);
    assert(a != flacl::List<int>());
    assert(a.back() == 17);
    a.insert(a.begin(),5);
    assert(a.invariant());
    assert(a.front() == 5);
    assert(a.back() == 17);
    assert(a == flacl::List<int>({5,7,8,9,17}));
    a.insert(std::next(a.begin()),6);
    assert(a.invariant());
    assert(a == flacl::List<int>({5,6,7,8,9,17}));
  }
  { // insertion, iterators
    flacl::List<int> a{};
    int i = 8;
    a.insert(a.end(),7);
    assert(a.invariant());
    a.insert(a.end(),std::move(i));
    assert(a.invariant());
    a.emplace(a.end(),9);
    assert(a.invariant());
    auto it = std::next(a.begin(),2);
    a.insert_after(it,11);
    assert(a.invariant());
    i = 10;
    a.insert_after(it,std::move(i));
    assert(a.invariant());
    assert(a == flacl::List<int>({7,8,9,10,11}));
  }
  { // erasure
    flacl::List<int> a{7,8,9,10,11};
    flacl::List<int> b{7,8,9,10,11};
    assert(a.invariant());
    assert(a == b);
    a.erase(a.begin());
    assert(a.invariant());
    assert(a == flacl::List<int>({8,9,10,11}));
    a.erase(std::next(a.begin()));
    assert(a.invariant());
  }
  { // swapping
    flacl::List<int> a{8,10,11};
    flacl::List<int> b{7,8,9,10,11};
    flacl::List<int> d{};
    flacl::List<int> const c{8,10,11};
    assert(a == c);
    assert(b != c);
    std::swap(a,b);
    assert(a != c);
    assert(b == c);
    a.swap(b);
    assert(a == c);
    assert(b != c);
    std::swap(a,a);
    assert(a == c);
    std::swap(a,d);
    assert(d == c);
    assert(a != c);
  }
  { // resize
    flacl::List<int> a({10,100,101});
    a.resize(1);
    assert((a == flacl::List<int>{10}));
    a.resize(2);
    assert((a == flacl::List<int>{10,0}));
    a.resize(4,11);
    assert((a == flacl::List<int>{10,0,11,11}));
    a.resize(3,100);
    assert((a == flacl::List<int>{10,0,11}));
  }
}
