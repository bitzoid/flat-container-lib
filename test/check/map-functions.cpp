#include "flacl/map.h"

#include <optional>

// this test does not actually check correctness but enumerates all API functions for gcov

int main() {
  flacl::UnorderedMap<int,std::optional<int>,6> um{{42,42}};
  flacl::UnorderedMap<int,std::optional<int>,6> const cum{{42,42}};
  {
    flacl::UnorderedMap<int,std::optional<int>,6> empty;
    flacl::UnorderedMap<int,std::optional<int>,6> copy = um;
    flacl::UnorderedMap<int,std::optional<int>,6> moved = std::move(copy);
    copy = std::move(moved);
    moved = copy;
    flacl::UnorderedMap<int,std::optional<int>,6> init{{0,0},{4,4},{2,2},{4,4}};
    init = {{0,0},{1,1},{2,2},{3,3}};
    flacl::UnorderedMap<int,std::optional<int>,6> iter(um.begin(),um.end());
  }
  { /* iterators */
    um.begin();
    cum.begin();
    um.cbegin();
    um.end();
    cum.end();
    um.cend();
  }
  { /* capacity */
    cum.empty();
    cum.size();
    cum.flat_size();
    cum.is_flat();
    cum.flat_size();
    cum.find(0);
    cum.contains(0);
    cum.count(0);
  }
  { /* modifiers */
    um.clear();
    um.swap(um);
    swap(um,um);
    um.erase(um.end());
    um.erase(100);
    um.insert(std::pair<int,std::optional<int>>{0,0});
    um.emplace(std::pair<int,std::optional<int>>{0,0});
    um.insert_nocheck(std::pair<int,std::optional<int>>{55,55});
    um.emplace_nocheck(std::pair<int,std::optional<int>>{77,77});
  }
  { /* comparison functions */
    um == cum;
    um != cum;
  }
  um[0];
  flatten(um);
}
