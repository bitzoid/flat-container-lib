#include "flacl/vector.h"

// this test does not actually check correctness but enumerates all API functions for gcov

int main() {
  {
    flacl::Vector<int,2> a1;
    int data[] = {0,1};
    flacl::Vector<int,2> a2(std::begin(data),std::end(data));
    flacl::Vector<int,2> a3({0,1});

    flacl::Vector<int,2> a4(a3);
    flacl::Vector<int,2> a5(std::move(a4));

    a4 = std::move(a5);
    a3 = a4;
    a2.assign(std::begin(data),std::end(data));
    a3.assign({0,1});
  }

  flacl::Vector<int,10> a({0,1,2,3,4,5,6,7,8,9});
  flacl::Vector<int,10> const ac(a);
  { /* element access */
    a[1] = ac[1];
    a.data()[2] = ac.data()[2];
    a.at(3) = ac.at(3);
    a.front() = ac.front();
    a.back() = ac.back();
  }

  { /* iterators */
    auto it = a.begin();
    auto itc = ac.begin();
    auto itcc = ac.cbegin();
    for (; it != a.end() && itc != ac.end() && itcc != ac.cend(); ++it, ++itc, ++itcc) {
      *it = *itc;
      *it = *itcc;
    }
  }
  {
    auto it = a.rbegin();
    auto itc = ac.rbegin();
    auto itcc = ac.crbegin();
    for (; it != a.rend() && itc != ac.rend() && itcc != ac.crend(); ++it, ++itc, ++itcc) {
      *it = *itc;
      *it = *itcc;
    }
  }

  { /* capacity */
    (void)a.capacity();
    a.reserve(0);
    (void)a.empty();
    (void)a.size();
  }

  { /* modifiers */
    a.clear();
    int const ci = 0;
    a.push_back(ci);
    int i = 0;
    a.push_back(std::move(i));
    a.pop_back();
    a.emplace_back(1);
    a.emplace_back(std::move(i));
    a.resize(a.size()+1);
    a.resize(a.size()+1,99);
    flacl::Vector<int,10> a2({42});
    swap(a2,a);
    a.swap(a2);
    a.swap(a2);
    a.emplace(a.begin(),0);
    a.insert(a.begin(),0);
    a.insert(a.end(),a2.begin(),a2.end());
    a.insert(a.end(),1,47);
    a.clear();
    a.insert(a.begin(),{0,1,2,3,4,5,6,7});
    a.erase(a.begin()+1);
    a.erase(a.begin()+2,a.begin()+5);
  }

  { /* comparison functions */
    a == ac;
    a != ac;
    a < ac;
    a <= ac;
    a > ac;
    a >= ac;
  }
}
