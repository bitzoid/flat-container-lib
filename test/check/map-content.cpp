#include "flacl/map.h"
#include <optional>

#include <cassert>

int main() {
  flacl::UnorderedMap<int,std::optional<int>,6> um;
  swap(um,um);
  assert(um.size() == 0);
  assert(um.find(0) == um.end());
  assert(um.size() == 0);
  assert(um[0] == std::optional<int>());
  assert(um.size() == 1);
  um[0] = 77;
  assert(um[0] == 77);
  assert(um.find(0) != um.end());
  assert(um.size() == 1);
  um[2] = 2;
  um[1] = 1;
  assert((um == flacl::UnorderedMap<int,std::optional<int>,3>{{0,77},{1,1},{2,2}}));
}
