#include "flacl/map.h"

#include "t.h"

int main() {
  flacl::UnorderedMap<T,T,4,T::about> ms;
  std::cout << ms.emplace(std::make_pair(10,10)).second << "\n";
  std::cout << ms.emplace(std::make_pair(20,20)).second << "\n";
  std::cout << ms.emplace(std::make_pair(30,30)).second << "\n";
  std::cout << ms.emplace(std::make_pair(14,14)).second << "\n";
  std::cout << ms.emplace(std::make_pair(39,39)).second << "\n";
  for (auto const& t: ms) {
    std::cout << t.first << "->" << t.second << "  ";
  }
  std::cout << "\n";
  ms.erase(10);
  for (auto const& t: ms) {
    std::cout << t.first << "->" << t.second << "  ";
  }
  std::cout << "\n";

  ms[40] = 40;
  ms[50] = 50;
  ms[60] = 60;
  ms[70] = 70;
  ms.erase(40);
  ms.erase(50);
  ms.erase(60);

  for (auto const& t: ms) {
    std::cout << t.first << "->" << t.second << "  ";
  }
  std::cout << "\n";

  std::cout << "is flat: " << ms.is_flat() << "\n";

  flacl::flatten(ms);

  std::cout << "is flat: " << ms.is_flat() << "\n";
}
