#include <iostream>

#include "flacl/vector.h"
#include "t.h"


int main() {
  using namespace flacl;
  Array<T,6> fv;
  std::cout << "size: " << fv.size() << "\n";
  std::cout << "max_size: " << fv.max_size() << "\n";
  std::cout << (fv.emplace_back(10)-fv.begin()) << "\n";
  std::cout << (fv.emplace_back(11)-fv.begin()) << "\n";
  std::cout << (fv.emplace_back(T(12))-fv.begin()) << "\n";
  std::cout << (fv.emplace_back(13)-fv.begin()) << "\n";
  std::cout << (fv.emplace_back(14)-fv.begin()) << "\n";
  std::cout << (fv.emplace_back(15)-fv.begin()) << "\n";
  std::cout << (fv.emplace_back(16)-fv.begin()) << "\n";
  std::cout << (fv.emplace_back(17)-fv.begin()) << "\n";
  std::cout << (fv.emplace_back(18)-fv.begin()) << "\n";

  for (auto const& i: fv) { std::cout << i << " "; } std::cout << " <-------------------------\n";

  {
    std::cout << "before move\n";
    Array<T,6> copy = std::move(fv);
    std::cout << "after move\n";
  }
  std::cout << "after scope end\n";
  std::cout << (fv.emplace(fv.end(),10)-fv.begin()) << "\n";
  std::cout << "10 emplaced\n";
  fv.emplace_back(11);
  auto twelve = fv.push_back(12);
  std::cout << (fv.emplace(fv.end(),13)-fv.begin()) << "\n";

  for (auto const& i: fv) { std::cout << i << " "; } std::cout << " <-------------------------\n";

  int values1[] = {100,101};
  fv.insert(twelve,std::begin(values1),std::end(values1));

  for (auto const& i: fv) { std::cout << i << " "; } std::cout << " <-------------------------\n";

  fv.pop_back();
  fv.pop_back();

  fv.insert(fv.end(),2,1000);

  for (auto const& i: fv) { std::cout << i << " "; } std::cout << " <-------------------------\n";

  fv.clear();

  fv.insert(fv.end(),{4,5,6});
  fv.assign({2,3});

  for (auto it = fv.crbegin(); it < fv.crend(); ++it) { std::cout << *it << " "; } std::cout << " <-------------------------\n";

  std::cout << "============\n";
  {
    Array<T,6> empty = {10,11,12,13,14};
    std::swap(empty,fv);
  }

  for (auto const& i: fv) { std::cout << i << " "; } std::cout << " <-------------------------\n";
  std::cout << "erase\n";
  fv.erase(fv.begin()+0, fv.begin()+2);
  std::cout << "erase DONE\n";
  for (auto const& i: fv) { std::cout << i << " "; } std::cout << " <-------------------------\n";
  fv.emplace_back(15);
  fv.erase(fv.begin(),fv.end());
  for (auto const& i: fv) { std::cout << i << " "; } std::cout << " <-------------------------\n";

  {
    Array<T,8> cmp = {4,7};
    fv.emplace_back(4);
    std::cout << (fv <= cmp) << "\n";
  }
}
