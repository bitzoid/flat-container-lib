#include "flacl/vector.h"
#include "flacl/set.h"
#include "flacl/list.h"

#include <chrono>
#include <iostream>
#include <iomanip>
#include <limits>
#include <utility>

#include <deque>
#include <unordered_set>
#include <set>
#include <list>

using chronores = std::micro;
static constexpr char const resstr[] = "us";
static constexpr std::size_t K = 1024;
static constexpr std::size_t R = 32;

namespace {
  std::string trim(std::string const& s) {
    std::string r;
    for (auto c: s) {
      if (c != ' ') r.push_back(c);
    }
    return r;
  }
}

namespace wrapper {
  template <typename C, typename T>
  auto find(C const& c, T const& t) {
    return std::find(c.begin(),c.end(),t);
  }
  template <typename T>
  auto find(std::set<T> const& c, T const& t) {
    return c.find(t);
  }
  template <typename T>
  auto find(std::unordered_set<T> const& c, T const& t) {
    return c.find(t);
  }
  template <std::size_t N, typename T>
  auto find(flacl::UnorderedSet<T,N> const& c, T const& t) {
    return c.find(t);
  }
  template <typename C, typename It, typename T>
  auto insert(C& c, It it, T const& t) {
    return c.insert(it,t);
  }
  template <typename It, typename T>
  auto insert(std::set<T>& c, It, T const& t) {
    return c.insert(t);
  }
  template <typename It, typename T>
  auto insert(std::unordered_set<T>& c, It, T const& t) {
    return c.insert(t);
  }
  template <std::size_t N, typename It, typename T>
  auto insert(flacl::UnorderedSet<T,N>& c, It, T const& t) {
    return c.insert(t);
  }
  template <typename C>
  auto lastit(C& c) {
    return std::prev(c.end());
  }
  template <typename T>
  auto lastit(std::unordered_set<T>& c) {
    return std::next(c.begin(),c.size()-1);
  }
}

namespace {
  struct Time {
    double& time;
    std::chrono::time_point<std::chrono::steady_clock> start;
    Time(double& time) : time(time) {
      start = std::chrono::steady_clock::now();
    }
    ~Time() {
      auto const finish = std::chrono::steady_clock::now();
      time += std::chrono::duration<double,chronores>(finish-start).count();
    }
  };

  template <typename Vec>
  struct ctor_iter {
    template <typename Vec::value_type::value_type... init>
    static double run(std::integer_sequence<typename Vec::value_type::value_type,init...>) {
      double tm = 0;
      typename Vec::value_type::value_type volatile data = 0;
      {
        Time timer(tm);
        for (std::size_t k = 0; k < K; ++k) {
          Vec vecs(512,typename Vec::value_type({init...}));
          for (auto& v: vecs) {
            for (auto& i: v) {
              data += i;
            }
          }
        }
      }
      return tm;
    }
  };

  template <typename Vec>
  struct ctor {
    template <typename Vec::value_type::value_type... init>
    static double run(std::integer_sequence<typename Vec::value_type::value_type,init...>) {
      double tm = 0;
      typename Vec::value_type::value_type volatile data = 0;
      {
        Time timer(tm);
        for (std::size_t k = 0; k < K; ++k) {
          Vec vecs(512,typename Vec::value_type({init...}));
          data += *vecs.begin()->begin();
        }
      }
      return tm;
    }
  };

  template <typename Vec>
  struct iter {
    template <typename Vec::value_type::value_type... init>
    static double run(std::integer_sequence<typename Vec::value_type::value_type,init...>) {
      double tm = 0;
      typename Vec::value_type::value_type volatile data = 0;
      {
        Vec vecs(512,typename Vec::value_type({init...}));
        Time timer(tm);
        for (std::size_t k = 0; k < K; ++k) {
          for (auto& v: vecs) {
            for (auto& i: v) {
              data += i;
            }
          }
        }
      }
      return tm;
    }
  };

  template <typename Vec>
  struct find_elem {
    template <typename Vec::value_type::value_type... init>
    static double run(std::integer_sequence<typename Vec::value_type::value_type,init...>) {
      double tm = 0;
      typename Vec::value_type::value_type volatile data = 0;
      {
        Vec vecs(512,typename Vec::value_type({init...}));
        Time timer(tm);
        for (std::size_t k = 0; k < K; ++k) {
          for (auto& v: vecs) {
            for (typename Vec::value_type::value_type needle: {0,3,10}) {
              if (auto it = wrapper::find(v,needle); it != v.end()) {
                data += *it;
              }
            }
          }
        }
      }
      return tm;
    }
  };

  template <typename Vec, bool stack>
  struct linear_use {
    template <typename Vec::value_type::value_type... init>
    static double run(std::integer_sequence<typename Vec::value_type::value_type,init...>) {
      double tm = 0;
      typename Vec::value_type::value_type data = 0;
      {
        Vec vecs(512,typename Vec::value_type({init...}));
        Time timer(tm);
        for (auto& v: vecs) {
          for (std::size_t k = 0; k < K; ++k) {
            auto it = v.begin();
            if constexpr (stack) {
              it = wrapper::lastit(v);
            }
            data = *it;
            v.erase(it);
            wrapper::insert(v,v.end(),data);
          }
        }
      }
      return tm;
    }
  };
  template <typename T, std::size_t N, bool stack>
  struct linear_use<std::vector<std::array<T,N>>,stack> {
    template <T... init>
    static double run(std::integer_sequence<T,init...>) {
      return std::numeric_limits<double>::quiet_NaN();
    }
  };
  template <typename Vec>
  using stack_use = linear_use<Vec,true>;
  template <typename Vec>
  using queue_use = linear_use<Vec,false>;
}

struct TimeFmt {
  std::size_t r;
  double t;
  template <typename Os>
  friend Os& operator<<(Os& os, TimeFmt const& fmt) {
    os << (fmt.t/K/(fmt.r?fmt.r:1)) << ((fmt.t==fmt.t)?resstr:std::string(""));
    return os;
  }
};

template <typename Content, std::size_t N, typename Test>
void runtest(std::string const& test, std::string const& name, Test const&) {
  double t = 0;
  Test::run(std::integer_sequence<Content, N>());
  std::string flushspace = "                        ";
  for (std::size_t i = 0; i < R; ++i) {
    std::cout << "> Testing " << test << "(" << trim(name) << ") ... " << i << " / " << R << " -> ~" << TimeFmt{i,t} << flushspace << "\r" << std::flush;
    t += Test::run(std::integer_sequence<Content, N>());
  }
  std::cout << "[" << test << "] " << name << ": " << std::fixed << std::setw( 8 ) << std::setprecision( 3 ) << TimeFmt{R,t} << flushspace << std::endl;
}

template <typename U, typename Ic>
using flaclVector = flacl::Vector<U,Ic::value>;

template <template <typename Vec> typename Test, typename Content, std::size_t N>
void run(std::string const& test) {
  runtest<Content,N>(test, "      std::set", Test<std::vector<std::set<Content>>>());
  runtest<Content,N>(test, "   std::un_set", Test<std::vector<std::unordered_set<Content>>>());
  runtest<Content,N>(test, "  flacl::UnSet", Test<std::vector<flacl::UnorderedSet<Content,N>>>());
  runtest<Content,N>(test, "     std::list", Test<std::vector<std::list<Content>>>());
  runtest<Content,N>(test, "   flacl::List", Test<std::vector<flacl::List<Content,flaclVector,std::integral_constant<std::size_t,N>>>>());
  runtest<Content,N>(test, "    std::deque", Test<std::vector<std::deque<Content>>>());
  runtest<Content,N>(test, "   std::vector", Test<std::vector<std::vector<Content>>>());
  runtest<Content,N>(test, " flacl::Vector", Test<std::vector<flacl::Vector<Content,N>>>());
  runtest<Content,N>(test, "    std::array", Test<std::vector<std::array<Content,N>>>());
  runtest<Content,N>(test, "  flacl::Array", Test<std::vector<flacl::Array<Content,N>>>());
  std::cout << "\n";
}


int main() {
  //run<ctor,int>("ctor");
  //run<iter,int>("iter");
  run<ctor_iter,int,8>("ctor_iter");
  run<find_elem,int,8>("find_elem");
  run<stack_use,int,8>("stack_use");
  run<queue_use,int,8>("queue_use");
}
