#include "flacl/set.h"

#include "t.h"

int main() {
  flacl::UnorderedSet<T,4,T::about> us;
  std::cout << us.emplace(10).second << "\n";
  std::cout << us.emplace(20).second << "\n";
  std::cout << us.emplace(30).second << "\n";
  std::cout << us.emplace(T{14}).second << "\n";
  std::cout << us.emplace(39).second << "\n";
  for (auto const& t: us) {
    std::cout << t << " ";
  }
  std::cout << "\n";
  us.erase(10);
  for (auto const& t: us) {
    std::cout << t << " ";
  }
  std::cout << "\n";

  std::cout << us.emplace(40).second << "\n";
  std::cout << us.emplace(50).second << "\n";
  std::cout << us.emplace(60).second << "\n";
  std::cout << us.emplace(70).second << "\n";
  us.erase(40);
  us.erase(50);
  us.erase(60);

  for (auto const& t: us) {
    std::cout << t << " ";
  }
  std::cout << "\n";

  std::cout << "is flat: " << us.is_flat() << "\n";

  flacl::flatten(us);

  std::cout << "is flat: " << us.is_flat() << "\n";
}
