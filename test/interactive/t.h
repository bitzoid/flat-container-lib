#pragma once

#include <iostream>

struct T {
  int i;
  ~T() { std::cout << "~dtor(" << i << ")\n"; i = -2; }
  T(int i = 0) : i(i) { std::cout << "ctor(" << i << ")\n"; }
  T(T const& from) : i(from.i) { std::cout << "ctor(const& " << i << ")\n"; }
  T(T&& from) : i(from.i) { std::cout << "ctor(&& " << i << ")\n"; from.i = -1; }
  T& operator=(T const& from) { std::cout << i << " = (const& " << from.i << ")\n"; i = from.i; return *this; }
  T& operator=(T&& from) { std::cout << i << " = (&& " << from.i << ")\n"; i = from.i; from.i = -1; return *this; }

  template <typename Os>
  friend Os& operator<<(Os& os, T const& t) {
    os << "T{" << t.i << "}";
    return os;
  }
  friend bool operator==(T const& lhs, T const& rhs) {
    return lhs.i == rhs.i;
  }
  friend bool operator<(T const& lhs, T const& rhs) {
    return lhs.i < rhs.i;
  }
  friend bool operator<=(T const& lhs, T const& rhs) {
    return lhs.i <= rhs.i;
  }

  struct about {
    bool operator()(T const& lhs, T const& rhs) const {
      return lhs.i/10 == rhs.i/10;
    }
  };
};
