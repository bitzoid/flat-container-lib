#include <iostream>

#include "flacl/vector.h"
#include "t.h"


int main() {
  std::cout << sizeof(flacl::Vector<std::size_t,0>) << "\n";
  std::cout << sizeof(flacl::Vector<std::size_t,1>) << "\n";
  std::cout << sizeof(flacl::Vector<std::size_t,2>) << "\n";
  std::cout << sizeof(flacl::Vector<std::size_t,125>) << "\n";

  flacl::Vector<int> v = {0,1,2,3,4};

  for (int i = 5; i < 20; ++i) {
    v.emplace_back(i);
    std::cout << "is flat: " << v.is_flat() << "\n";
  }

  for (auto const& i: v) { std::cout << i << " "; } std::cout << " <-------------------------\n";

  v.erase(std::find(v.begin(),v.end(),2),v.end());

  for (auto const& i: v) { std::cout << i << " "; } std::cout << " <-------------------------\n";

  std::cout << "is flat: " << v.is_flat() << "\n";

  flacl::flatten(v);

  std::cout << "is flat: " << v.is_flat() << "\n";
}
