#include <iostream>

#include "flacl/vector.h"

struct E {
  E(int) {}
  E(E const&) = delete;
  E(E&&) = default;
  E& operator=(E const&) = delete;
  E& operator=(E&&) = delete;
};

struct S {
  flacl::Vector<E,4> fvec;
  S() = default;
  S(S const&) = delete;
  S(S&&) = default;
  S& operator=(S const&) = delete;
  S& operator=(S&&) = default;
};

int main() {
  std::vector<S> structs;
  structs.emplace_back();
}
