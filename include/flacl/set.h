#pragma once

/*
 *  flat-container-lib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* This header-only library provides the following type:
 *
 *   flacl::UnorderedSet<T,[N],[Equal],[BaseAlloc]>
 *       A contiguous container implementing an unordered set according to Equal.
 *       The container uses a flacl::Vector<T,N,BaseAlloc> as underlying container for the actual data.
 *       Hence, once more than N elements are inserted the object ceases to be flat and space corresponding to N Ts will be unusable for the remaining lifetime of the object.
 *
 */

#include "vector.h"

#include <utility>

namespace flacl {
  template <typename T, std::size_t N = std::size_t(-1), typename Equal = std::equal_to<T>, template <typename> typename BaseAlloc = std::allocator>
  class UnorderedSet : private Equal {
    private:
      using V = Vector<T,N,BaseAlloc>;
    public:
      using key_type                = typename V::value_type;
      using value_type              = typename V::value_type;
      using size_type               = typename V::size_type;
      using difference_type         = typename V::difference_type;
      using reference               = typename V::reference;
      using const_reference         = typename V::const_reference;
      using pointer                 = typename V::pointer;
      using const_pointer           = typename V::const_pointer;
      using iterator                = typename V::const_iterator; // !
      using const_iterator          = typename V::const_iterator;

      using key_equal               = Equal;

    private:
      V vec;

    protected:
      constexpr pointer remove_const(const_iterator it) noexcept {
        return vec.data() + (it - cbegin());
      }

    public:
      ~UnorderedSet() {}
      UnorderedSet(Equal equal = Equal()) : Equal(equal), vec() {}
      UnorderedSet(UnorderedSet const& other) : Equal(static_cast<Equal const&>(other)), vec(other.vec) { }
      UnorderedSet(UnorderedSet&& other) : Equal(std::move(static_cast<Equal&&>(other))), vec(std::move(other.vec)) { }
      UnorderedSet(std::initializer_list<value_type> ilist) : Equal(), vec() {
        *this = ilist;
      }
      template <typename It>
      UnorderedSet(It first, It last, Equal equal = Equal()) : Equal(equal), vec() {
        for (auto it = first; it < last; ++it) {
          insert(*it);
        }
      }
      UnorderedSet& operator=(UnorderedSet const& other) {
        Equal::operator=(other);
        vec = other.vec;
        return *this;
      }
      UnorderedSet& operator=(UnorderedSet&& other) {
        Equal::operator=(std::move(other));
        vec = std::move(other.vec);
        return *this;
      }
      UnorderedSet& operator=(std::initializer_list<value_type> ilist) {
        for (auto const& v: ilist) {
          insert(v);
        }
        return *this;
      }

      /* iterators */
      constexpr iterator begin() noexcept { return vec.begin(); }
      constexpr const_iterator begin() const noexcept { return vec.begin(); }
      constexpr const_iterator cbegin() const noexcept { return vec.cbegin(); }
      iterator end() noexcept { return vec.end(); }
      const_iterator end() const noexcept { return vec.end(); }
      const_iterator cend() const noexcept { return vec.cend(); }

      /* capacity */
      bool empty() const {
        return vec.empty();
      }
      size_type size() const {
        return vec.size();
      }

      bool is_flat() const noexcept {
        return vec.is_flat();
      }
      constexpr size_type flat_size() const noexcept {
        return vec.flat_size();
      }

      /* lookup */
      template <typename U>
      const_iterator find(U const& key) const {
        return std::find_if(vec.begin(),vec.end(),[&](T const& x) { return this->Equal::operator()(x,key); });
      }
      template <typename U>
      bool contains(U const& key) const {
        return find(key) != end();
      }
      template <typename U>
      size_type count(U const& key) const {
        return contains(key)?1ul:0ul;
      }

      /* modifiers */
      void clear() {
        vec.clear();
      }
      void swap(UnorderedSet& other) {
        vec.swap(other.vec);
      }
      friend void swap(UnorderedSet& lhs, UnorderedSet& rhs) {
        lhs.swap(rhs);
      }

      iterator erase(const_iterator pos) {
        if (pos >= end()) {
          return end();
        }
        if (auto last = std::prev(end()); last != pos) {
          *remove_const(pos) = std::move(*remove_const(last));
        }
        vec.pop_back();
        return pos;
      }
      template <typename U>
      size_type erase(U const& key) {
        auto const it = find(key);
        if (it == end()) {
          return 0;
        }
        erase(it);
        return 1;
      }

      template <typename U>
      std::pair<iterator,bool> insert(U&& value) {
        if (auto it = find(std::forward<U>(value)); it != end()) {
          return std::make_pair(it,false);
        }
        vec.push_back(std::forward<U>(value));
        return std::make_pair(std::prev(end()),true);
      }

      template <typename... Args>
      std::pair<iterator,bool> emplace(Args&&... args) {
        vec.emplace_back(std::forward<Args>(args)...);
        if (auto it = find(vec.back()); it < std::prev(end())) {
          vec.pop_back();
          return std::make_pair(it,false);
        } else {
          return std::make_pair(it,true);
        }
      }

      template <typename U>
      iterator insert_nocheck(U&& value) {
        vec.push_back(std::forward<U>(value));
        return std::prev(end());
      }

      template <typename... Args>
      iterator emplace_nocheck(Args&&... args) {
        vec.emplace_back(std::forward<Args>(args)...);
        return std::prev(end());
      }

      static bool flatten(UnorderedSet& us) {
        return V::flatten(us.vec);
      }

      /* comparison functions */
      template <size_type M>
      friend bool operator==(UnorderedSet const& lhs, UnorderedSet<T,M,Equal,BaseAlloc> const& rhs) {
        if (lhs.size() != rhs.size()) return false;
        for (auto const& l: lhs) {
          if (!rhs.contains(l)) return false;
        }
        return true;
      }
      template <size_type M>
      friend bool operator!=(UnorderedSet const& lhs, UnorderedSet<T,M,Equal,BaseAlloc> const& rhs) {
        return !(lhs == rhs);
      }

  };
}
