#pragma once

/*
 *  flat-container-lib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* This header-only library provides the following two types:
 *
 *   flacl::UnorderedMap<K,V,N,[Equal],[BaseAlloc]>
 *       A contiguous container implementing an unordered map according to Equal.
 *       The container uses a flacl::Vector<T,N,BaseAlloc> as underlying container for the actual data.
 *       Hence, once more than N elements are inserted the object ceases to be flat and space corresponding to N K+Vs will be unusable for the remaining lifetime of the object.
 *
 */

#include "set.h"

namespace flacl {

  namespace detail {
    template <typename K, typename V, typename KeyEqual>
    struct PairEqual : KeyEqual {

      template <typename U>
      static U const& key(U const& k) { return k; }
      static K const& key(std::pair<K,V> const& kv) { return kv.first; }

      template <typename Lhs, typename Rhs>
      bool operator()(Lhs const& lhs, Rhs const& rhs) const {
        return KeyEqual::operator()(key(lhs),key(rhs));
      }
    };
  }

  template <typename K, typename V, std::size_t N, typename KeyEqual = std::equal_to<K>, template <typename> typename BaseAlloc = std::allocator>
  class UnorderedMap : private UnorderedSet<std::pair<K,V>,N,detail::PairEqual<K,V,KeyEqual>,BaseAlloc> {
    private:
      using S = UnorderedSet<std::pair<K,V>,N,detail::PairEqual<K,V,KeyEqual>,BaseAlloc>;
    public:
      using key_type                = K;
      using value_type              = V;
      using size_type               = typename S::size_type;
      using difference_type         = typename S::difference_type;
      using reference               = typename S::reference;
      using const_reference         = typename S::const_reference;
      using pointer                 = typename S::pointer;
      using const_pointer           = typename S::const_pointer;
      using iterator                = typename S::const_iterator; // !
      using const_iterator          = typename S::const_iterator;

      using key_equal               = typename S::key_equal;

    private:

    public:
      ~UnorderedMap() {}
      UnorderedMap() = default;
      UnorderedMap(UnorderedMap const&) = default;
      UnorderedMap(UnorderedMap&&) = default;
      UnorderedMap(std::initializer_list<typename S::value_type> ilist) : S(ilist) {}
      template <typename It>
      UnorderedMap(It first, It last) : S(first,last) {}

      UnorderedMap& operator=(UnorderedMap const&) = default;
      UnorderedMap& operator=(UnorderedMap&&) = default;

      /* iterators */
      using S::begin;
      using S::cbegin;
      using S::end;
      using S::cend;

      /* capacity */
      using S::empty;
      using S::size;

      using S::is_flat;
      using S::flat_size;

      /* lookup */
      using S::find;
      using S::contains;
      using S::count;

      /* modifiers */
      using S::clear;
      using S::swap;
      void swap(UnorderedMap& other) {
        S::swap(other);
      }
      friend void swap(UnorderedMap& lhs, UnorderedMap& rhs) {
        lhs.swap(rhs);
      }

      using S::erase;
      using S::insert;
      using S::emplace;
      using S::insert_nocheck;
      using S::emplace_nocheck;

      template <typename U>
      value_type& operator[](U const& u) {
        auto const it = find(u);
        if (it == end()) {
          return S::remove_const(insert_nocheck(std::make_pair(u,V())))->second;
        } else {
          return S::remove_const(it)->second;
        }
      }

      static bool flatten(UnorderedMap& um) {
        return S::flatten(um);
      }

      /* comparison functions */
    private:
      template <typename K2, typename V2, std::size_t N2, typename KeyEqual2, template <typename> typename BaseAlloc2>
      friend class UnorderedMap;

      template <size_type M>
      bool equal_map(UnorderedMap<K,V,M,KeyEqual,BaseAlloc> const& rhs) const {
        return static_cast<S const&>(*this) == static_cast<typename UnorderedMap<K,V,M,KeyEqual,BaseAlloc>::S const&>(rhs);
      }
    public:
      template <size_type M>
      friend bool operator==(UnorderedMap const& lhs, UnorderedMap<K,V,M,KeyEqual,BaseAlloc> const& rhs) {
        return lhs.equal_map(rhs);
      }
      template <size_type M>
      friend bool operator!=(UnorderedMap const& lhs, UnorderedMap<K,V,M,KeyEqual,BaseAlloc> const& rhs) {
        return !(lhs == rhs);
      }
  };
}
