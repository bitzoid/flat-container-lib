#pragma once

/*
 *  flat-container-lib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* This header-only library provides the following type:
 *
 *   flacl::List<T,[VectorLike]>
 *       A contiguous container implementing a linked list.
 *       By default, the container uses a flacl::Vector<U> as underlying container for the actual data, but it can be replaced by e.g. std::vector<U,A> or flacl::Vector<U,N,A> or any vector-like thing.
 *       To use the latter, instantiate the type as List<T,flacl::Vector,N,A>. The first template argument must be a value_type to use.
 *
 */

#include "vector.h"

#include <initializer_list>
#include <limits>
#include <functional>
#include <algorithm>
#include <type_traits>
#include <cassert>

namespace flacl {
  namespace detail {
    template <typename T, std::size_t N, template <typename> typename BaseAlloc>
    class List {
      public:
        using value_type       = T;
        using size_type        = typename Vector<T, N, BaseAlloc>::size_type;
        using difference_type  = typename Vector<T, N, BaseAlloc>::difference_type;

      protected:
        static constexpr size_type npos = std::numeric_limits<size_type>::max();
        struct NodeBase {
          size_type prev = List::npos;
          size_type next = List::npos;
        };
        struct Node : NodeBase {
          T content = std::nullopt;
        };
        using V = Vector<Node, N, BaseAlloc>;

        static_assert(std::is_same_v<typename V::size_type,size_type>, "Used vector sub-container does not have the same size_type for different content.");
        static_assert(std::is_same_v<typename V::difference_type,difference_type>, "Used vector sub-container does not have the same difference_type for different content.");

      private:
        size_type sz = 0;
        NodeBase root{}; // {.prev = tail, .next = head}
        V data = {};

        inline size_type head() const noexcept {
          return root.next;
        }
        inline size_type tail() const noexcept {
          return root.prev;
        }
        inline static NodeBase& node_base(Node& nd) noexcept {
          return static_cast<NodeBase&>(nd);
        }
        inline static NodeBase const& node_base(Node const& nd) noexcept {
          return static_cast<NodeBase const&>(nd);
        }
        template <typename Lt>
        inline static auto& node_helper(Lt& list, size_type i) noexcept {
          [[unlikely]] if (i == Lt::npos) {
            return list.root;
          } else {
            return node_base(list.data[i]);
          }
        }
        inline NodeBase const& node(size_type i) const noexcept {
          return List::node_helper<std::add_const_t<List<T,N,BaseAlloc>>>(*this, i);
        }
        inline NodeBase& node(size_type i) noexcept {
          return List::node_helper<List<T,N,BaseAlloc>>(*this, i);
        }

        template <typename L, typename Tcv, bool fwd = true>
        class Iterator {
          friend List;
          private:
            L* l;
            size_type i;
            Iterator(L* l, size_type i) : l(l), i(i) {}
          public:
            using value_type = Tcv;
            using pointer = Tcv*;
            using reference = Tcv&;
            using difference_type = typename L::difference_type;
            using iterator_category = std::bidirectional_iterator_tag;

            operator Iterator<std::add_const_t<L>,std::add_const_t<Tcv>,fwd>() const {
              return Iterator<std::add_const_t<L>,std::add_const_t<Tcv>,fwd>(l,i);
            }

            friend bool operator==(Iterator const& l, Iterator const& r) {
              return l.i == r.i;
            }
            friend bool operator!=(Iterator const& l, Iterator const& r) {
              return !(l == r);
            }
            auto& operator*() const {
              return l->data[i].content;
            }
            auto& operator++() {
              if constexpr (fwd) {
                i = l->node(i).next;
              } else {
                i = l->node(i).prev;
              }
              return *this;
            }
            auto operator++(int) {
              auto const it = *this;
              ++*this;
              return it;
            }
            auto& operator--() {
              if constexpr (fwd) {
                i = l->node(i).prev;
              } else {
                i = l->node(i).next;
              }
              return *this;
            }
            auto operator--(int) {
              auto const it = *this;
              --*this;
              return it;
            }
        };

      public:
        using allocator_type   = typename V::allocator_type;
        using reference        = value_type&;
        using const_reference  = value_type const&;
        using pointer          = value_type*;
        using const_pointer    = value_type const*;

        using iterator                 = Iterator<List, value_type, true>;
        using const_iterator           = Iterator<List const, value_type const, true>;
        using reverse_iterator         = Iterator<List, value_type, false>;
        using const_reverse_iterator   = Iterator<List const, value_type const, false>;

      private:
        inline constexpr iterator mkit(size_type i) {
          return iterator(this,i);
        }
        inline constexpr const_iterator mkit(size_type i) const {
          return const_iterator(this,i);
        }
        inline constexpr iterator mkit(const_iterator it) {
          return mkit(it.i);
        }
        inline constexpr const_iterator mkit(iterator it) const {
          return mkit(it.i);
        }
        inline constexpr reverse_iterator mkitr(size_type i) {
          return reverse_iterator(this,i);
        }
        inline constexpr const_reverse_iterator mkitr(size_type i) const {
          return const_reverse_iterator(this,i);
        }

        iterator chain_in(size_type k, const_iterator it) {
          assert(k != npos); /* GCOVR_EXCL_LINE */
          data[k].next = it.i;
          data[k].prev = node(it.i).prev;
          node(data[k].prev).next = k;
          node(data[k].next).prev = k;
          ++sz;
          return mkit(k);
        }
        iterator chain_in(const_iterator it) {
          return chain_in(sz,it);
        }

        iterator chain_out(size_type k) {
          assert(k != npos); /* GCOVR_EXCL_LINE */
          iterator prev = std::prev(mkit(k));
          iterator next = std::next(mkit(k));
          node(prev.i).next = next.i;
          node(next.i).prev = prev.i;
          --sz;
          return next;
        }

      public:
        List() {}
        List(List const&) = default;
        List(List&&) = default;
        List& operator=(List const&) = default;
        List& operator=(List&&) = default;
        ~List() {}

        template <typename A>
        List(std::initializer_list<A> il) {
          if (il.size()) {
            data.reserve(il.size());
            for (auto it = il.begin(); it != il.end(); ++it) {
              data.emplace_back(Node{{data.size()-1,data.size()+1},*it});
            }
            data.front().prev = npos;
            data.back().next = npos;
            sz = il.size();
            root.next = 0;
            root.prev = sz-1;
          }
        }

        List(size_type n) : List() {
          resize(n);
        }

        List(size_type n, const_reference value) : List() {
          resize(n,value);
        }

        // Element access

        reference front() {
          return data[head()].content;
        }
        const_reference front() const {
          return data[head()].content;
        }
        reference back() {
          return data[tail()].content;
        }
        const_reference back() const {
          return data[tail()].content;
        }

        // Iterators

        iterator begin() { return mkit(head()); }
        const_iterator begin() const { return mkit(head()); }
        const_iterator cbegin() const { return mkit(head()); }
        iterator end() { return mkit(npos); }
        const_iterator end() const { return mkit(npos); }
        const_iterator cend() const { return mkit(npos); }

        reverse_iterator rbegin() { return mkitr(tail()); }
        const_reverse_iterator rbegin() const { return mkitr(tail()); }
        const_reverse_iterator crbegin() const { return mkitr(tail()); }
        reverse_iterator rend() { return mkitr(npos); }
        const_reverse_iterator rend() const { return mkitr(npos); }
        const_reverse_iterator crend() const { return mkitr(npos); }

        // Capacity

        bool empty() const noexcept {
          return sz == 0;
        }
        size_type size() const noexcept {
          return sz;
        }
        constexpr size_type max_size() const noexcept {
          return npos-1;
        }
        size_type capacity() const noexcept {
          return data.capacity();
        }
        void reserve(size_type n) {
          data.reserve(n);
        }

        // Modifiers

        void clear() {
          sz = 0;
          root = NodeBase();
          data.clear();
        }

        void resize(size_type const n) {
          reserve(n);
          while (n > sz) {
            emplace_back();
          }
          while (n < sz) {
            pop_back();
          }
        }

        void resize(size_type const n, const_reference value) {
          reserve(n);
          while (n > sz) {
            push_back(value);
          }
          while (n < sz) {
            pop_back();
          }
        }

        iterator insert(const_iterator it, T const& val) {
          data.emplace_back(Node{NodeBase(),val});
          return chain_in(it);
        }
        iterator insert(const_iterator it, T&& val) {
          data.emplace_back(Node{NodeBase(),std::move(val)});
          return chain_in(it);
        }
        iterator insert_after(const_iterator it, T const& val) {
          return insert(++it,val);
        }
        iterator insert_after(const_iterator it, T&& val) {
          return insert(++it,std::move(val));
        }

        template <typename... Args>
        iterator emplace(const_iterator it, Args&&... args) {
          data.emplace_back(Node{NodeBase(),T(std::forward<Args>(args)...)});
          return chain_in(it);
        }

        reference push_back(T const& val) {
          return *insert(end(),val);
        }
        reference push_back(T&& val) {
          return *insert(end(),std::move(val));
        }
        template <typename... Args>
        reference emplace_back(Args&&... args) {
          return *emplace(end(),std::forward<Args>(args)...);
        }

        reference push_front(T const& val) {
          return *insert(begin(),val);
        }
        reference push_front(T&& val) {
          return *insert(begin(),std::move(val));
        }
        template <typename... Args>
        reference emplace_front(Args&&... args) {
          return *emplace(begin(),std::forward<Args>(args)...);
        }

        iterator erase(const_iterator it, const_iterator* check_invalidate = nullptr) {
          iterator const last = mkit(sz-1);
          auto const ret = chain_out(it.i);
          if (it != last) {
            iterator const lastn = chain_out(last.i);
            chain_in(it.i,lastn);
            //std::cerr << "moving [" << last.i << "] = " << *last << " to [" << it.i << "] = " << *it << "\n";
            *mkit(it) = std::move(*last);
            if (check_invalidate && mkit(*check_invalidate) == last) {
              *check_invalidate = it;
            }
          }
          data.pop_back();
          return ret;
        }

        iterator erase(const_iterator first, const_iterator last) {
          iterator ret = mkit(last);
          bool more = first != last;
          while (more) {
            auto const prev = std::prev(ret);
            assert(sz); /* GCOVR_EXCL_LINE */
            more = first != prev;
            ret = erase(prev,&first);
          }
          return ret;
        }

        void pop_back() {
          erase(std::prev(end()));
        }

        void pop_front() {
          erase(begin());
        }

        void swap(List& other) {
          std::swap(sz,other.sz);
          std::swap(root,other.root);
          std::swap(data,other.data);
        }
        friend void swap(List& lhs, List& rhs) {
          return lhs.swap(rhs);
        }

        // comparison functions

        friend bool operator==(List const& l, List const& r) {
          if (l.size() != r.size()) return false;
          return std::equal(l.begin(),l.end(),r.begin());
        }
        friend bool operator!=(List const& l, List const& r) {
          return !(l == r);
        }
        friend bool operator<(List const& l, List const& r) {
          return std::lexicographical_compare(l.begin(),l.end(),r.begin(),r.end(),std::less<T>());
        }
        friend bool operator<=(List const& l, List const& r) {
          return std::lexicographical_compare(l.begin(),l.end(),r.begin(),r.end(),std::less_equal<T>());
        }
        friend bool operator>(List const& l, List const& r) {
          return std::lexicographical_compare(l.begin(),l.end(),r.begin(),r.end(),std::greater<T>());
        }
        friend bool operator>=(List const& l, List const& r) {
          return std::lexicographical_compare(l.begin(),l.end(),r.begin(),r.end(),std::greater_equal<T>());
        }


        bool invariant() const {
          /* GCOVR_EXCL_START */
          //std::cerr << "invariant()\n";
          //std::cerr << "  looks empty from left iff looks empty from right\n";
          if ((node(end().i).prev == npos) != (node(end().i).next == npos)) return false;
          //std::cerr << "  looks empty elements iff empty\n";
          if (node(end().i).prev == npos) return empty();

          auto it = begin();
          //std::cerr << "  check begin is not end if not empty\n";
          if (it == end()) return false;
          // check link end <-> begin
          //std::cerr << "  check link end <-> begin\n";
          if (node(node(npos).next).prev != npos) return false;
          size_type checksum_good = 0;
          size_type checksum_idxs = 0;
          for (size_type checked = 0; checked < sz; ++checked, ++it) {
            //std::cerr << "  @" << it.i << ":\n";
            //std::cerr << "    num elements == sz\n";
            if (it == end()) return false;
            //std::cerr << "    check link it <-> it.next\n";
            if (node(node(it.i).next).prev != it.i) return false;
            checksum_good += checked;
            checksum_idxs += it.i;
          }
          //std::cerr << "  checksum: are all vector elements visited at some point (checks for sub-loops)\n";
          //std::cerr << "    good = " << checksum_good << ", idxs = " << checksum_idxs << ", diff = " << (static_cast<difference_type>(checksum_good) - static_cast<difference_type>(checksum_idxs)) << "\n";
          if (checksum_good != checksum_idxs) return false;
          //std::cerr << "  check that no extra elements past the expected end\n";
          if (it != end()) return false;
          //std::cerr << "all looks fine\n";
          /* GCOVR_EXCL_STOP */
          return true;
        }
    };

    template <typename T, std::size_t N, template <typename> typename BaseAlloc>
    class OptListN_impl : private List<T, 0, BaseAlloc> {
      private:
        using Node = typename List<T,0,BaseAlloc>::Node;
      public:
        static constexpr std::size_t value = detail::OptVecN<Node, N, BaseAlloc>;
    };

    template <typename T, std::size_t N, template <typename> typename BaseAlloc>
    inline constexpr std::size_t OptListN = OptListN_impl<T,N,BaseAlloc>::value;
  }

  template <typename T, std::size_t N = std::size_t(-1), template <typename> typename BaseAlloc = std::allocator>
  using List = detail::List<T,detail::OptListN<T,N,BaseAlloc>,BaseAlloc>;
}
