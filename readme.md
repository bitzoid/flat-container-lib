# flacl -- flat container library

## This header-only library provides the following types:

### `flacl::Array<T,N>`

  A contiguous container with fully automatic storage. Is limited to holding N elements. Wraps std::array.
  The interface of Array closely mirrors that of std::vector, but inserting past N elements will not succeed.
  
  **Performance**: Array has performance comparable to std::array. It uses `sizeof(size_t)` more bytes.

### `flacl::Vector<T,N=...,A=...>`

  A contiguous container optimised to use N automatic storage locations and resorts to dynamic memory if more is required. Wraps std::vector.
  The container will use the nested buffer of N elements and hence requires no allocations of dynamic memory until that point.
  Once more data is inserted the container switches over to the regular behaviour of std::vector. This container is a generalised version of `Array<T,N>` as it can grow past N.
  
  Note: Once the required data spills over, the internal storage is dead weight.
  An instance of `Vector<T>` (with default N) attemts to exhaust the size of *one cache line*.
  
  **Performance**: Vector has some overhead compared to std::array and may deteriorate to the performance of std::vector.

## General notes

The helper types and constants in namespace detail should not be used from outside this library.
The library is encapsulation-safe: It can be wrapped namespaces arbitrarily.

## Performance benchmark (construction + iteration)

`       std::set:  179.806us`  
`    std::un_set:  208.945us`  
`   flacl::UnSet:   12.991us`  
`      std::list:   41.764us`  
`    flacl::List:   41.823us`  
`     std::deque:  166.488us`  
`    std::vector:   31.538us`  
`  flacl::Vector:   13.340us`  
`     std::array:    5.738us`  
`   flacl::Array:    5.837us`  

## License

> flat-container-lib
> Copyright (C) 2020  bitzoid
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author

*  bitzoid {zoid at riseup dot net}
